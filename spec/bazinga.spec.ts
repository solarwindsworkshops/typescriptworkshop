

describe("Bazinga", () => {

    beforeEach(() => {
        this.bazinga = new app.Bazinga();
    })

    it("do something", () => {
        var result = this.bazinga.doIt();

        expect(result).toBe(28);
    })

    it("yet another test", () => {

        spyOn(this.bazinga, "doIt").and.returnValue(11);
        
        var resutl = this.bazinga.doIt();

        expect(resutl).toBe(11);
        expect(this.bazinga.doIt).toHaveBeenCalledTimes(1);
    })
})