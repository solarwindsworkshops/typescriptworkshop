module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            'src/*.ts',
            'spec/*.spec.ts'
        ],
        preprocessors: {
            '**/*.ts': ['typescript']
        },

        typescriptPreprocessor: {
            options: {
                sourceMap: false, // generate source maps
                noResolve: false // enforce type resolution
            },
            transformPath: function (path) {
                return path.replace(/\.ts$/, '.js');
            }
        },
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: false,
        concurrency: Infinity
    })
}